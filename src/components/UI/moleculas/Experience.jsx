import { Divider, Typography } from "@mui/material";
import React from "react";
import styles from './Moleculas.module.scss'

export default function Experience() {
    return (
        <div className={styles.tabContainer}>
            <div className="flex flex-col space-y-4 overflow-y-auto max-h-[30em]">
                <div>
                    <div className="flex justify-between py-2 font-display items-center">
                        <p className="text-white"><a href="https://erp.deepen.uz/auth/sign-in" target="__blank" className="underline decoration-wavy hover:decoration-pink-500 hover:text-pink-500">Deepen</a> | Frontend Developer</p>
                        <p className="text-white text-sm italic">May 2024 - Present</p>
                    </div>
                    <Divider />
                    <div className="py-2 max-w-[90%]">
                        <ul className="text-text-color font-display text-sm !list-disc !list-outside">
                            <li>
                                🌟 At Deepen, I contributed to the development of an admin panel designed to streamline the operations of fitness centers. My role focused on creating user-friendly interfaces that allowed fitness centers to efficiently manage client visits, check-ins, and payments. I worked closely with the team to ensure smooth functionality, optimize performance, and enhance the overall user experience, enabling better client engagement and operational management.
                            </li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div className="flex justify-between py-2 font-display items-center">
                        <p className="text-white"><a href="https://exadot.io/" target="__blank" className="underline decoration-wavy hover:decoration-pink-500 hover:text-pink-500">Exadot</a> | Frontend Developer</p>
                        <p className="text-white text-sm italic">June 2023 - April 2024</p>
                    </div>
                    <Divider />
                    <div className="py-2 max-w-[90%]">
                        <ul className="text-text-color font-display text-sm !list-disc !list-outside">
                            <li>
                                🚀 Crafting single-page applications with meticulous attention to detail for achieving pixel-perfect views.</li>
                            <li>
                                🚀 Collaborating closely with designers and backend developers to achieve a smooth integration of frontend and backend functionalities.</li>
                            <li>
                                🚀 Employing state-of-the-art technologies like Zustand and Zod to enhance the development process.
                            </li>
                            <li>
                                🚀 Executing the implementation of responsive and mobile-friendly designs for an optimal user experience.</li>
                            <li>

                                🚀 Enhancing website performance through the application of advanced techniques such as lazy loading, code splitting, and caching.</li>
                            <li>
                                🚀 Proficient in utilizing technologies such as Zustand, Zod, Chakra, Next.js, and React.js to deliver high-quality solutions.</li>

                        </ul>
                    </div>
                </div>
                <div>
                    <div className="flex justify-between py-2 font-display items-center">
                        <p className="text-white"><a href="https://udevs.io/" target="__blank" className="underline decoration-wavy hover:decoration-pink-500 hover:text-pink-500">Udevs</a> | Frontend Developer</p>
                        <p className="text-white text-sm italic">June 2021 - May 2023</p>
                    </div>
                    <Divider />
                    <div className="py-2 max-w-[90%]">
                        <ul className="text-text-color font-display text-sm !list-disc">
                            <li>🏆 Over 2 years of experience in Developing and maintaining frontend codebase for web applications using React, NextJs, and Redux</li>
                            <li>🏆 Strong experience in multi-language (Localization) support, panel, and dark-mode support for the site</li>
                            <li>🏆 Strong experience in Updating the old version of the framework and libraries to the latest ones</li>
                            <li>🏆 Collaborating with backend developers to integrate frontend with backend APIs using REST API and GraphQL</li>
                            <li>🏆 Conducting code reviews and providing feedback to team members to ensure code quality and consistency</li>
                            <li>🏆 Implementing responsive designs and optimizing application performance using CSS/SCSS and JavaScript</li>
                            <li>🏆 Working with UX designers to create intuitive and aesthetically pleasing UI designs using MUI, Tailwind, Chakra</li>
                            <li>🏆 Completed 5 real projects and worked on admin dashboards.</li>
                        </ul>
                    </div>
                </div>
                <div >
                    <div className="flex justify-between py-2 font-display items-center">
                        <p className="text-white"><a href="https://www.upwork.com/freelancers/~013ad0fa526ac8ee70" className="underline decoration-wavy hover:decoration-pink-500 hover:text-pink-500">UPWORK</a> | Freelancer</p>
                        <p className="text-white text-sm italic">Feb 2022 - Present</p>
                    </div>
                    <Divider />
                    <div className="py-2 max-w-[90%]">
                        <p className="mb-2 font-medium font-display text-sm text-white/80">As a freelance Frontend Developer, I worked on building a web platform called PureCalculators. The platform consists of a series of calculators designed to calculate different types of fields. My responsibilities included:</p>

                        <ul className="text-text-color font-display text-sm !list-disc">
                            <li>🤝 Designing and implementing the frontend of the platform using HTML, CSS, and JavaScript</li>
                            <li>🤝 Developing the user interface and user experience for the calculators to ensure ease of use</li>
                            <li>🤝 Working with the client to gather requirements and translate them into functional features on the platform</li>
                            <li>🤝 Utilizing responsive design techniques to ensure the platform is accessible across multiple devices</li>
                            <li>🤝 Collaborating with the backend developer to integrate the frontend and backend functionalities</li>
                            <li>🤝 Testing the platform to ensure its compatibility with different web browsers and devices</li>
                            <li>🤝 Utilizing version control systems such as Git and Github for code management</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div className="flex justify-between py-2 font-display items-center">
                        <p className="text-white"><a href="https://www.jafton.com/" className="underline decoration-wavy hover:decoration-pink-500 hover:text-pink-500">Jafton</a> | PM Assistant</p>
                        <p className="text-white text-sm italic">Dec 2020 - October 2021</p>
                    </div>
                    <Divider />
                    <div className="py-2 max-w-[90%]">
                        <p className="mb-2 font-medium font-display text-sm text-white/80">As an Assistant Product Manager at Missed.com, I played a critical role in supporting product managers with various tasks related to product development and launch. My responsibilities included:</p>
                        <ul className="text-text-color font-display text-sm !list-disc">
                            <li>💼 Conducting user research to gather insights on user needs and pain points, and identify areas for improvement in the product.</li>
                            <li>💼 Creating and maintaining product roadmaps to guide the development and launch of new features and enhancements to the product.</li>
                            <li>💼 Working closely with cross-functional teams such as designers and developers to ensure timely delivery of products, and that the product functionality and user experience met requirements.</li>
                            <li>💼 Collaborating with product managers to develop and launch new features for the product, utilizing user stories and product requirements to guide development.</li>
                            <li>💼 Conducting A/B testing to optimize product performance and ensure user needs were met.</li>
                            <li>💼 Conducting market research to identify potential growth areas for the product, and to inform product development and marketing strategies.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
