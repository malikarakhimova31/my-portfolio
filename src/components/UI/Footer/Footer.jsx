import styles from './style.module.scss'
import Link from 'next/link'
import { Container, Typography } from '@mui/material'
import { GithubIcon, TwitterIcon } from '../Icons'
import { BsLinkedin, BsInstagram, BsTelegram } from 'react-icons/bs'

import { MdOutlineFileDownload } from "react-icons/md"


export function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.leftBox}>
        <Typography variant='h5'>find me in:</Typography>
        <div className={styles.iconBox}>
          <Link href="https://instagram.com/frontend_surfing?igshid=ZGUzMzM3NWJiOQ==" passHref>
            <a target='__blank'>
              <BsInstagram fontSize="18px" fill='var(--text-color)' />
            </a>
          </Link>
        </div>
        <div className={styles.iconBox}>
          <Link href="https://www.linkedin.com/in/malika-rakhimova/" passHref>
            <a target='__blank'>
              <BsLinkedin fontSize="18px" fill='var(--text-color)' />
            </a>
          </Link>
        </div>
        <div className={styles.iconBox}>
          <Link href="https://t.me/themelosh" passHref>
            <a target='__blank'>
              <BsTelegram fontSize="20px" fill='var(--text-color)' />
            </a>
          </Link>
        </div>
        {/* <div className={styles.iconBox}><BsInstagram fontSize="20px" fill='var(--text-color)'/></div> */}
      </div>
      <div className={styles.rightBoxWrapper}>
        <div className={`${styles.rightBox} cursor-pointer`}>
          <a href="/CV-2024.pdf" download="Malika-Rakhimova-CV">
            <Typography variant='h5'>Get CV</Typography>
          </a>
          <MdOutlineFileDownload fontSize="20px" fill='var(--text-color)' />
        </div>
        <div className={styles.rightBox}>
          <a href="https://github.com/MalikaRakhimova31" target='__blank'>
            <Typography variant='h5'>@MalikaRakhimova31</Typography>
          </a>
          <GithubIcon />
        </div>
      </div>
    </footer>
  )
}
