export const bio = [
  {
    order: "1",
    text: "/**",
  },
  {
    order: "2",
    text: "* About me",
  },
  {
    order: "3",
    text: "* I am an experienced Frontend Developer",
  },
  {
    order: "4",
    text: "* with 3 years of experience building . ",
  },
  {
    order: "5",
    text: "* commercial projects. Mainly skilled in ",
  },
  {
    order: "6",
    text: "* Javascript, React, Redux, NextJS, Material UI,",
  },
  {
    order: "7",
    text: "* Tailwind CSS and other minor skills that",
  },
  {
    order: "8",
    text: "* required for building websites.",
  },
  {
    order: "9",
    text: "* I am meticulous developer in responsive design",
  },
  {
    order: "10",
    text: "* can create pixel perfect web sites.",
  },
  {
    order: "11",
    text: "**/",
  },
];
