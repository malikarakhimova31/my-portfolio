export const projects = [
  {
    name: "UCafè",
    src: "/images/menu.webp",
    link: "https://ucafe.uz/",
    bg: "white",
    fit: "cover",
  },
  {
    name: "GrandPharm",
    src: "/images/grand.webp",
    link: "https://grandpharm.uz/",
    bg: "white",
    fit: "cover",
  },
  {
    name: "Cspace",
    src: "/images/cspace.png",
    link: "https://cspace.uz/",
    bg: "white",
    fit: "contain",
  },
  {
    name: "ZuzuPizza",
    src: "/images/zuzu.png",
    link: "https://zuzupizza.uz/",
    bg: "white",
    fit: "contain",
  },
  {
    name: "Evos",
    src: "/images/evos.png",
    link: "https://evos.uz/en",
    bg: "white",
    fit: "contain",
  },
  {
    name: "Admin Panel",
    src: "/images/erecipe.jpg",
    link: "https://youtu.be/DUNAwV5vxsI",
    bg: "white",
    fit: "cover",
  },
];
