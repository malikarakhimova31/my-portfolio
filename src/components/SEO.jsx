import React from 'react'
import Head from 'next/head'

export default function SEO({ title }) {
  return (
    <Head>
      <meta
        name='viewport'
        content='width=device-width, initial-scale=1  maximum-scale=1, user-scalable=0'
      />
      <meta charSet='utf-8' />
      <title>{title || 'Frontend Developer'}</title>
      <meta name='description' content='Next js' />
      <meta
        name='keywords'
        content='A highly skilled Fullstack Developer with experience in Python, Django, and React seeking to utilize my expertise in developing efficient and scalable web applications.'
      />
      <meta property='og:type' content='website' />
      <meta
        property='og:title'
        content={title || 'Frontend Developer'}
        key='ogtitle'
      />
      <meta property='og:description' content='A highly skilled Fullstack Developer with experience in Python, Django, and React seeking to utilize my expertise in developing efficient and scalable web applications.' key='ogdesc' />
      <meta
        property='og:site_name'
        content={title || 'Frontend Developer'}
        key='ogsitename'
      />
      <meta
        property='og:image'
        content='/seo.jpg'
        key='ogimage'
      />
      <meta name='twitter:card' content='summary' />
      <meta name='twitter:title' content={title || 'Frontend Developer'} />
      <meta name='twitter:description' content='A highly skilled Fullstack Developer with experience in Python, Django, and React seeking to utilize my expertise in developing efficient and scalable web applications.' />
      <meta name='twitter:site' content={title || 'Frontend Developer'} />
      <meta name='twitter:creator' content='Malika Rakhimova' />
      <meta
        name='twitter:image'
        content='/seo.jpg'
      />

      <link rel='icon' href='/code.png' />
      {/* <link
        href='https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Source+Sans+Pro:wght@300;600;700&display=swap'
        rel='stylesheet'
      /> */}
    </Head>
  )
}
